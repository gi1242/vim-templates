# templates: Neovim plugin to insert blank templates

By default, if you edit a new file with filetype `foo`, then this plugin will
look for the skeleton file in the `templates` directory of your configuration
folder (usually `~/.config/nvim/templates` in Linux). If found, this skeleton
file will be inserted. Complicated rules selecting the skeleton file can be
configured.

## Installation instructions

If you use [lazy.nvim](https://lazy.folke.io/) then install this plugin by:

    -- Templates when editing new files
    { 'https://gitlab.com/gi1242/vim-templates.git', opts={
      patterns={
        -- If creating a file of filetype foo, the skeleton file `skel.foo` will be
        -- used if it exists. Fine grained rules can be configured here.
        -- All skeleton files should be in the directory
        -- `stdpath('config')/templates`
        { 'hw*.tex', 'homework.tex' }, -- overrides skel.tex
        { 'sec{-,}[1-9]{,*}.tex', nil }, -- Use nil for no template
      }
    }}

## Configuration

If configuring using `patterns` during setup (as above) isn't enough, you can
create your own autocommands:

    au BufNewFile {pattern} InsertTemplate <filename>

The `InsertTemplate` command only inserts a skeleton file if the current buffer
is non-empty. So if your custom auto-commands match more than once, only the
first matching one will have effect.
