-- Vim plugin to insert blank templates
-- Maintainer:	    GI <gi1242+vim@nospam.com> (replace nospam with gmail)
-- Created:	    Mon 16 Mar 2015 12:54:55 PM EDT
-- Last Changed:    Fri 10 Jan 2025 09:39:57 AM EST
-- Version:	0.2
local M = {}
M.patterns = {
  -- { glob, template }
  -- or { glob=glob, template=template }
  -- Use a nil template to disable
}

local function insert_template(file)
  --echomsg 'insert_template( file=' . a:file . ')'

  -- Don't do anything for non-empty buffers
  if vim.fn.line( '$' ) > 1 or vim.fn.getline( 1 ) ~= ''
      or vim.g.insert_template_disable
      or vim.b.insert_template_disable
  then
    return
  end

  local template_dir = vim.fn.stdpath('config')..'/templates'
  local skel = file or ('skel.' .. vim.bo.filetype)

  local filename = template_dir .. '/' .. skel
  if vim.fn.filereadable( filename ) ~= 0 then
    local cpoptions = vim.o.cpoptions
    vim.opt.cpoptions:remove('a')
    vim.cmd( 'read ' .. filename )
    vim.cmd( 'normal kdd' )
    vim.o.cpoptions = cpoptions
    vim.notify( 'Inserted template '.. skel, vim.log.levels.INFO, {} )
  end
end

function M.setup( opts )
  if type( opts.patterns ) == "table" then
    for _, v in ipairs( opts.patterns ) do
      local glob = v.glob or v[1]
      if glob:find( '/' ) == nil then
	glob = '**/' .. glob
      end
      table.insert( M.patterns,
	{ lpeg=vim.glob.to_lpeg(glob), template=v.template or v[2] } )
    end
  end
end

vim.api.nvim_create_user_command( 'InsertTemplate',
  function (Opts) insert_template( Opts.fargs[1] ) end,
  {nargs=1} )

vim.api.nvim_create_autocmd( 'BufNewFile', {
  pattern = '*',
  callback = function (args)
    for _, v in ipairs( M.patterns ) do
      if v.lpeg:match( args.match ) then
	return v.template and insert_template( v.template )
      end
    end
    return insert_template()
  end
})

return M
